package com.techstern.technokids;

import android.app.ActivityOptions;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.techstern.technokids.model.LoginModel;
import com.techstern.technokids.services.JsonObjectHandler;
import com.techstern.technokids.session.LoginSession;
import com.techstern.technokids.utility.AlertMessage;
import com.techstern.technokids.utility.AppConstant;
import com.techstern.technokids.utility.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    EditText edt_user_name;
    EditText edt_password;
    Button btn_login;
    String user_name,password;
    AlertMessage alertMessage;
    ConnectionDetector detector;
    ProgressDialog progressDialog;
    JsonObjectHandler handler;

    LoginModel loginModel;
    LoginSession login_session;

    int show_password=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        initView();
    }

    public void  initView()
    {
        edt_user_name=(EditText)findViewById(R.id.edt_user_name_loginActivity);
        edt_password=(EditText)findViewById(R.id.edt_password_loginActivity);
        btn_login=(Button)findViewById(R.id.btn_loginActivity);

        //initialization
        alertMessage=new AlertMessage();
        detector=new ConnectionDetector(this);
        progressDialog=new ProgressDialog(this);
        handler=new JsonObjectHandler();
        loginModel=new LoginModel();
        login_session=new LoginSession();


        loginModel = login_session.getPreferences(LoginActivity.this, AppConstant.login_session);

        if(loginModel!=null)
        {
          startActivity(new Intent(LoginActivity.this,StudentListAcitivity.class));
        }

        //listner
        btn_login.setOnClickListener(this);



        edt_password.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edt_password.getRight() - edt_password.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        show_password++;

                        if(show_password%2==0)
                        {
                            edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0,R.mipmap.ic_visibility , 0);
                            edt_password.setTransformationMethod(new PasswordTransformationMethod());

                        }
                        else
                        {
                            edt_password.setCompoundDrawablesWithIntrinsicBounds(0, 0,R.mipmap.ic_visibility_off, 0);
                            edt_password.setTransformationMethod(null);
                        }

                        return true;
                    }
                }
                return false;
            }
        });
    }

    public void onClick(View v) {

        user_name =edt_user_name.getText().toString();
        password=edt_password.getText().toString();

        switch (v.getId())
        {
            case R.id.btn_loginActivity:

                validation();

                break;

        }
    }

    public void validation()
    {
        if(user_name.length()==0 || password.length()==0)
        {
            if(user_name.length()==0)
            {
                alertMessage.showAlertDialog(LoginActivity.this,"Alert!!","User Name should not be blank",false);
            }
            else
            {
                alertMessage.showAlertDialog(LoginActivity.this,"Alert!!","Password should not be blank",false);
            }
        }
        else if(!detector.isConnectingToInternet())
        {
            alertMessage.showAlertDialog(LoginActivity.this,"Alert!!","Please check your Internet",false);
        }
        else
        {

            LoginApi loginApi = new LoginApi();
            loginApi.execute();
        }
    }

    /*......................calling the api for login....................................*/
    public class LoginApi extends AsyncTask<Void,Void,JSONObject>
    {
        String full_name, name, pass, roll_name, school_id, school_name;
        String access_token, message, status_code, status;
        String user_image, user_full_image,user_id;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

          progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            HashMap<String,String> data=new HashMap<>();

            JSONObject jsonObject=null;
            data.put("UserName",user_name);
            data.put("Password",password);
            try {

                jsonObject = handler.makeHttpRequest(AppConstant.login_url, AppConstant.post_method, data);

                Log.d("login response.....", jsonObject.toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                LoginActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        alertMessage.showAlertDialog(LoginActivity.this,"Alert!!","Slow Internet..Timed Out",false);
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();

            try
            {
             JSONObject security_info=jsonObject.getJSONObject("SecurityInfo");

                message=security_info.getString("Message");
                access_token=security_info.getString("AccessToken");
                status_code=security_info.getString("StatusCode");
                status=security_info.getString("Status");

                if(status==AppConstant.isSuccess)
                {
                    JSONObject accessInfo=jsonObject.getJSONObject("AccessInfo");

                    name=accessInfo.getString("UserName");
                    full_name=accessInfo.getString("FullName");
                    pass=accessInfo.getString("Password");
                    user_id=accessInfo.getString("UserID");
                    roll_name=accessInfo.getString("RoleName");
                    school_id=accessInfo.getString("SchoolID");
                    school_name=accessInfo.getString("SchoolName");
                    user_image=accessInfo.getString("ImgURL");
                    user_image=user_image.substring(1,user_image.length());
                    user_full_image=AppConstant.domain+user_image;

                    loginModel=new LoginModel();

                    loginModel.name=name;
                    loginModel.full_name=full_name;
                    loginModel.user_id=user_id;
                    loginModel.roll_name=roll_name;
                    loginModel.school_id=school_id;
                    loginModel.school_name=school_name;
                    loginModel.user_full_image=user_full_image;
                    loginModel.access_token=access_token;
                    loginModel.name=name;

                    login_session.setPreferences(LoginActivity.this,AppConstant.login_session,loginModel);

                     startActivity(new Intent(LoginActivity.this,StudentListAcitivity.class));
                }
                else
                {
                    alertMessage.showAlertDialog(LoginActivity.this,"Alert!!","Invalid UserName/Password",false);
                }

            }
            catch (JSONException ex)
            {
                ex.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }

}
