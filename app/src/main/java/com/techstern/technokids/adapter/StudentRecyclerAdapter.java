package com.techstern.technokids.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.techstern.technokids.AnswerSheetActivity;
import com.techstern.technokids.R;
import com.techstern.technokids.model.AnswerSheetModel;
import com.techstern.technokids.model.LoginModel;
import com.techstern.technokids.model.StudentListModel;
import com.techstern.technokids.services.JsonObjectHandler;
import com.techstern.technokids.session.LoginSession;
import com.techstern.technokids.utility.AlertMessage;
import com.techstern.technokids.utility.AppConstant;
import com.techstern.technokids.utility.RecyclerViewAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by IMRAN on 3/9/2017.
 */

public class StudentRecyclerAdapter extends RecyclerView.Adapter<StudentRecyclerAdapter.MyViewHolder> {

    public String task_status;
    private List<StudentListModel> itemList;
    Context studentListActivity;
    ProgressDialog progressDialog;
    AlertMessage alertMessage;
    JsonObjectHandler handler;
    LoginModel loginModel;
    LoginSession loginSession;
    String total_sheet_count;
    AnswerSheetModel answerSheetModel;
    public static List<AnswerSheetModel> answerSheetModelList;

    private ArrayList<StudentListModel> arraylist;

    public class MyViewHolder extends RecyclerView.ViewHolder  {
        public TextView name,grade,sheet,barcode,date;
        public RelativeLayout rel_list_row;



        public MyViewHolder(View view) {
            super(view);

            name = (TextView) view.findViewById(R.id.text_name_id);
            grade = (TextView) view.findViewById(R.id.text_grade_id);
            sheet = (TextView) view.findViewById(R.id.text_sheet_id);
            barcode = (TextView) view.findViewById(R.id.text_barcode_id);
            date = (TextView) view.findViewById(R.id.text_date_id);
            rel_list_row=(RelativeLayout)view.findViewById(R.id.rel_list_row_studentView);



        }

    }

    public StudentRecyclerAdapter()
    {}

    public StudentRecyclerAdapter(List<StudentListModel> itemList, Context context) {
        this.itemList = itemList;
        this.studentListActivity=context;

        progressDialog=new ProgressDialog(studentListActivity);
        alertMessage=new AlertMessage();
        handler=new JsonObjectHandler();
        loginModel=new LoginModel();
        loginSession=new LoginSession();
        answerSheetModelList=new ArrayList<>();
        arraylist=new ArrayList<>();

        this.arraylist.addAll(itemList);

    }

    @Override
    public StudentRecyclerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.student_list_view, parent, false);

        return new StudentRecyclerAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentRecyclerAdapter.MyViewHolder holder, final int position) {

        StudentListModel item = itemList.get(position);

        holder.name.setText(item.student_name);

        holder.grade.setText(item.grade_section);

        holder.sheet.setText(item.img_serial_no);

        holder.barcode.setText(item.barcode_id);


        String dateInString = item.created_date;
        String date_format = dateInString.substring(0,10);



        holder.date.setText(date_format);

        RecyclerViewAnimation.animate(holder, true);

        task_status=item.task_status;

        if(task_status.equals("Assign"))
        {
            holder.rel_list_row.setBackgroundResource(R.drawable.list_yellow_bg);
        }
        else
        {
            holder.rel_list_row.setBackgroundResource(R.drawable.list_green_bg);
        }

        holder.rel_list_row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String access_token;
                String school_grade_section_id;
                String student_id;
                String bar_code_id;
                String created_date;



                StudentListModel studentListModel = itemList.get(position);

                student_id=studentListModel.student_id;
                school_grade_section_id=studentListModel.school_grade_section_association_id;
                bar_code_id=studentListModel.barcode_id;
                created_date=studentListModel.created_date;
                total_sheet_count=studentListModel.img_serial_no;

                loginModel = loginSession.getPreferences(studentListActivity, AppConstant.login_session);
                access_token=loginModel.access_token;

                AnswerSheetApi answerSheetApi=new AnswerSheetApi(access_token,school_grade_section_id,student_id,bar_code_id,created_date);
                answerSheetApi.execute();

            }
        });

    }


    @Override
    public int getItemCount() {
        return itemList.size();
    }


    public class AnswerSheetApi extends AsyncTask<Void,Void,JSONObject>
    {

        String access_token;
        String school_grade_section_id;
        String student_id;
        String bar_code_id;
        String created_date;
        String answer_sheet_data;

        AnswerSheetApi(String access_token,String school_grade_section_id,String student_id,String bar_code_id,String created_date)
        {
            this.access_token=access_token;
            this.school_grade_section_id=school_grade_section_id;
            this.student_id=student_id;
            this.bar_code_id=bar_code_id;
            this.created_date=created_date;

            answer_sheet_data="?SchoolGradeSectionAssociationID=" + school_grade_section_id + "&UserID=" + student_id + "&BarcodeId=" + bar_code_id + "&CreateDate=" + created_date + "&AccessToken=" + access_token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            HashMap<String,String> data=new HashMap<>();

            JSONObject jsonObject=null;

            data.put("SchoolGradeSectionAssociationID",school_grade_section_id);
            data.put("UserId",student_id);
            data.put("BarcodeId",bar_code_id);
            data.put("CreateDate",created_date);
            data.put("AccessToken",access_token);

            final Activity context=(Activity)studentListActivity;

            try {

                jsonObject = handler.makeHttpRequest(AppConstant.answer_sheet_url, AppConstant.post_method, data);

                Log.d("login response.....", jsonObject.toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                context.runOnUiThread(new Runnable() {
                    public void run() {
                        alertMessage.showAlertDialog(context,"Alert!!","Slow Internet..Timed Out",false);
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();

            String message,access_token,status_code,status;
            String school_id, student_name, grade, section, school_grade_section_id, barcode_id;
            String evaluationImgURL, evaluatedMarks, totalMark, evaluatedBy, evaluationDate;
            String createdDate, createdBy, sheet_status,user_id,student_association_no;
            String image_url,answerSheetId,img_srl_no,img_full_url, evaluate_img_full_url="";

            try
            {
                JSONObject security_info=jsonObject.getJSONObject("SecurityInfo");

                message=security_info.getString("Message");
                access_token=security_info.getString("AccessToken");
                status_code=security_info.getString("StatusCode");
                status=security_info.getString("Status");

                if(status==AppConstant.isSuccess)
                {
                    JSONArray accessInformation=jsonObject.getJSONArray("answerSheetFiles");

                    answerSheetModelList.clear();

                    for(int i=0;i<accessInformation.length();i++) {

                        JSONObject accessInfo = accessInformation.getJSONObject(i);

                        answerSheetId = accessInfo.getString("AnswerSheetID");
                        school_id = accessInfo.getString("SchoolID");
                        student_name = accessInfo.getString("StudentName");
                        grade = accessInfo.getString("GradeName");
                        section = accessInfo.getString("SectionName");
                        school_grade_section_id = accessInfo.getString("SchoolGradeSectionAssociationID");
                        barcode_id = accessInfo.getString("BarcodeID");
                        img_srl_no = accessInfo.getString("ImgSrno");
                        user_id = accessInfo.getString("UserID");
                        student_association_no = accessInfo.getString("StudentAssociationNo");
                        image_url = accessInfo.getString("ImgURL");
                        evaluationImgURL = accessInfo.getString("EvaluationImgURL");
                        evaluatedMarks = accessInfo.getString("EvaluatedMarks");
                        totalMark = accessInfo.getString("TotalMark");
                        evaluatedBy = accessInfo.getString("EvaluatedBy");
                        evaluationDate = accessInfo.getString("EvaluationDate");
                        createdBy = accessInfo.getString("CreatedBy");
                        createdDate = accessInfo.getString("CreatedDate");
                        sheet_status = accessInfo.getString("Status");

                        image_url=image_url.substring(1,image_url.length());
                        img_full_url=AppConstant.domain+image_url;
                        if (!evaluationImgURL.equals("null"))
                        {
                            evaluationImgURL = evaluationImgURL.substring(1, evaluationImgURL.length());
                            evaluate_img_full_url = AppConstant.domain + evaluationImgURL;
                        }
                        else
                        {
                            evaluate_img_full_url="";
                        }

                        answerSheetModel=new AnswerSheetModel();

                        answerSheetModel.answerSheetId=answerSheetId;
                        answerSheetModel.school_id=school_id;
                        answerSheetModel.student_name=student_name;
                        answerSheetModel.grade=grade;
                        answerSheetModel.section=section;
                        answerSheetModel.school_grade_section_id=school_grade_section_id;
                        answerSheetModel.barcode_id=barcode_id;
                        answerSheetModel.img_srl_no=img_srl_no;
                        answerSheetModel.user_id=user_id;
                        answerSheetModel.img_full_url=img_full_url;
                        answerSheetModel.evaluate_img_full_url=evaluate_img_full_url;
                        answerSheetModel.student_association_no=student_association_no;
                        answerSheetModel.evaluatedMarks=evaluatedMarks;
                        answerSheetModel.totalMark=totalMark;
                        answerSheetModel.evaluatedBy=evaluatedBy;
                        answerSheetModel.evaluationDate=evaluationDate;
                        answerSheetModel.createdBy=createdBy;
                        answerSheetModel.createdDate=createdDate;
                        answerSheetModel.sheet_status=sheet_status;

                        answerSheetModelList.add(answerSheetModel);
                    }

                    Intent intent=new Intent(studentListActivity,AnswerSheetActivity.class);
                    intent.putExtra("total_Sheet",total_sheet_count);
                    studentListActivity.startActivity(intent);

                }
            }
            catch (JSONException ex)
            {
                ex.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }


    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        itemList.clear();
        if (charText.length() == 0) {
            itemList.addAll(arraylist);
        } else {
            for (StudentListModel dd : arraylist) {
                if (dd.grade_section.toLowerCase(Locale.getDefault())
                        .contains(charText)) {
                    itemList.add(dd);
                }

            }
        }
        notifyDataSetChanged();
    }

}
