package com.techstern.technokids.model;

/**
 * Created by IMRAN on 3/9/2017.
 */

public class StudentListModel {

    public String school_id, student_name, grade_name, section_name, barcode_id;
    public String school_grade_section_association_id, user_id, img_serial_no;
    public String evaluated_marks, total_marks, status_obj, created_date,grade_section;
    public String access_token,student_id,task_status;
}
