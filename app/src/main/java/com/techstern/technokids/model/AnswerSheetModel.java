package com.techstern.technokids.model;

/**
 * Created by IMRAN on 3/9/2017.
 */

public class AnswerSheetModel {
    public String school_id, student_name, grade, section, school_grade_section_id, barcode_id;
    public String evaluationImgURL, evaluatedMarks, totalMark, evaluatedBy, evaluationDate;
    public String createdDate, createdBy, sheet_status,user_id,student_association_no;
    public String image_url,answerSheetId,img_srl_no,img_full_url, evaluate_img_full_url="";
}
