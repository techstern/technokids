package com.techstern.technokids.session;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.techstern.technokids.LoginActivity;
import com.techstern.technokids.model.LoginModel;

/**
 * Created by IMRAN on 3/8/2017.
 */

public class LoginSession {
    public void setPreferences(Context context, String key, LoginModel value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("loginSession", Context.MODE_PRIVATE).edit();

        Gson gson = new Gson();
        String json = gson.toJson(value);

        editor.putString(key, json);

        editor.commit();

    }

    public LoginModel getPreferences(Context context, String key) {

        SharedPreferences prefs = context.getSharedPreferences("loginSession",	Context.MODE_PRIVATE);

        Gson gson = new Gson();
        String json = prefs.getString(key, "");
        LoginModel obj = gson.fromJson(json, LoginModel.class);
        return obj;
    }
}
