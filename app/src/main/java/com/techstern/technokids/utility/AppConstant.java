package com.techstern.technokids.utility;

/**
 * Created by IMRAN on 3/8/2017.
 */

public class AppConstant {

    /*......................App Url..........................*/
    public static String domain = "http://techno-manage.com";
    public static String domain_name = domain+"/api/WebApi/";
    public static String login_url=domain_name+ "apiAccount/Login";
    public static String student_list_url = domain_name + "ApiAnswerSheet/TeacherAnswerList";
    public static String answer_sheet_url = domain_name + "ApiAnswerSheet/AnswerViewer";
    public static String evaluation_marks_url = domain_name+"ApiAnswerSheet/AnswerEvaluate";

    /*......................App Calling Method..........................*/
    public static String get_method = "GET";
    public static String post_method = "POST";

    /*..........................check Status...............................*/
    public static String isSuccess = "true";
    /*.........................session_keys..............................*/
    public static final String login_session="login_session";
}
