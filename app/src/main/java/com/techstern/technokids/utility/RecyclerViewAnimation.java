package com.techstern.technokids.utility;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.support.v7.widget.RecyclerView;

/**
 * Created by IMRAN on 3/9/2017.
 */

public class RecyclerViewAnimation {

    public static void animate(RecyclerView.ViewHolder holder, boolean goesDown){
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animatorTranslateX = ObjectAnimator.ofFloat(holder.itemView, "translationX",
                50,0);
        animatorTranslateX.setDuration(1500);

        ObjectAnimator animatorTranslateY = ObjectAnimator.ofFloat(holder.itemView,"translationY",20,-20,10,-10,5,-5);
        animatorTranslateY.setDuration(1500);


        animatorSet.playTogether(animatorTranslateX,animatorTranslateY);
        animatorSet.start();

    }
}
