package com.techstern.technokids.utility;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import com.techstern.technokids.R;

/**
 * Created by IMRAN on 3/8/2017.
 */

public class AlertMessage {

    public void showAlertDialog(Context context, String title, String message,
                                Boolean status)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context,R.style.dialog_style);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(message);
        if(status != null)
            // Setting alert dialog icon
            alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }

    public void showAlertDialog(final Context context, String title, String message,
                                Boolean status, final int event)
    {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

        // Setting Dialog Title
        alertDialog.setTitle(title);

        alertDialog.setCancelable(false);

        // Setting Dialog Message
        alertDialog.setMessage(message);
        if(status != null)
            // Setting alert dialog icon
            alertDialog.setIcon((status) ? R.drawable.success : R.drawable.fail);

        // Setting OK Button
        alertDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                if(event==1) {
                    dialog.dismiss();
                    Activity activity = (Activity) context;
                    activity.finish();

                   // activity.startActivity(new Intent(activity, StudentListAcitivity.class));
                }
            }
        });
        // Showing Alert Message
        alertDialog.show();
    }
}
