package com.techstern.technokids;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.gesture.Gesture;
import android.gesture.GestureLibraries;
import android.gesture.GestureLibrary;
import android.gesture.GestureOverlayView;
import android.gesture.Prediction;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.techstern.technokids.adapter.StudentRecyclerAdapter;
import com.techstern.technokids.model.AnswerSheetModel;
import com.techstern.technokids.model.LoginModel;
import com.techstern.technokids.services.JsonObjectHandler;
import com.techstern.technokids.session.LoginSession;
import com.techstern.technokids.utility.AlertMessage;
import com.techstern.technokids.utility.AppConstant;
import com.techstern.technokids.utility.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class AnswerSheetActivity extends AppCompatActivity implements GestureOverlayView.OnGesturePerformedListener{

    GestureLibrary mLibrary;
    GestureOverlayView gestures;
    RelativeLayout me;
    String result;
    int currentTotalMarks = 0;
    float x;
    float y;
    TextView tv_evaluated_marks;
    TextView tv_current_page_marks;
    EditText edt_total_marks;
    GestureOverlayView screenshot;
    Button btn_save;
    ImageView img_view_answerSheet;
    ProgressDialog progressDialog;
    JsonObjectHandler handler;
    String img_url,img_evaluated_url;
    byte[] byteArray;
    LoginModel loginModel;
    LoginSession loginSession;
    int sheet_count=0;
    boolean bool1=true;
    int total_sheet=0;
    String final_result="";
    String result1;
    boolean pass=false;
    boolean isPass=false;
    TextView text;
    int count=0;
    boolean switch_status;
    ImageButton btn_all_clear,btn_last_clear;
    int final_currentTotalMarks=0;
    Boolean bool = true;
    int tempresult ;
    int final_total=0;
    List<AnswerSheetModel> answerSheetModelList;
    AlertMessage alertMessage;
    ImageLoader imageLoader;
    TextView tv_total_page,tv_page_no;
    Button btn_prev;
    int reset_all_marks;
    String eval_marks="0";
    String uploaded_total_marks,uploaded_evaluated_marks;
    String marks="0";
    TextView tv_final_marks;
    Switch switch_digit;
    ScaleGestureDetector scaleGestureDetector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_sheet);

        initView();
    }

    public void initView()
    {
        me = (RelativeLayout) findViewById(R.id.my_relative);
        screenshot = (GestureOverlayView) findViewById(R.id.gestures);
        tv_evaluated_marks = (TextView) findViewById(R.id.current_mark_id);
        btn_save = (Button) findViewById(R.id.save_button_id);
        edt_total_marks=(EditText)findViewById(R.id.edt_total_marks_answerSheetActivity);
        img_view_answerSheet=(ImageView)findViewById(R.id.img_sheet_answerSheetActivity);
        btn_all_clear=(ImageButton)findViewById(R.id.btn_all_clear_answerSheetActivity);
        btn_last_clear=(ImageButton)findViewById(R.id.btn_last_clear_answerSheetActivity);
        tv_total_page=(TextView)findViewById(R.id.tv_total_page_answerSheetActivity);
        tv_page_no=(TextView)findViewById(R.id.tv_page_no_answerSheetActivity);
        tv_current_page_marks=(TextView)findViewById(R.id.tv_current_page_marks_answerSheetActivity);
        btn_prev=(Button)findViewById(R.id.btn_prev_answerSheetActivity);
        tv_final_marks=(TextView)findViewById(R.id.tv_final_marks_answerSheetActivity);
        switch_digit =(Switch)findViewById(R.id.switch_done_answerSheetActivity);

        mLibrary = GestureLibraries.fromRawResource(this, R.raw.gesture);
        if (!mLibrary.load()) {
            finish();
        }

        //............Initialization
        answerSheetModelList=new ArrayList<>();
        progressDialog=new ProgressDialog(this);
        handler=new JsonObjectHandler();
        alertMessage=new AlertMessage();
        loginModel=new LoginModel();
        loginSession=new LoginSession();
        imageLoader=new ImageLoader(AnswerSheetActivity.this);


        me.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                if(isPass==false){
                    isPass=true;
                }else if(isPass==true){
                    isPass=false;
                }

                return false;
            }
        });


        btn_last_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(text!=null) {
                    text.setVisibility(View.GONE);
                }
                if(bool) {
                    int current_total= final_currentTotalMarks - tempresult;
                    if(current_total>0) {
                        tv_evaluated_marks.setText(current_total + "");
                        currentTotalMarks=currentTotalMarks-tempresult;
                    }
                    else
                    {
                        tv_evaluated_marks.setText(current_total + "");
                    }
                    bool= false;
                }
            }
        });


        btn_all_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               allGestureRelease();
            }
        });

        Bundle intent=getIntent().getExtras();
        total_sheet=Integer.parseInt(intent.getString("total_Sheet","N/A"));

           PrevButton();

            if(!uploaded_evaluated_marks.equals("null")) {
                tv_evaluated_marks.setText(uploaded_evaluated_marks);
                reset_all_marks=Integer.parseInt(uploaded_evaluated_marks);
            }
            if(!uploaded_total_marks.equals("null")) {
                edt_total_marks.setText(uploaded_total_marks);
                edt_total_marks.setEnabled(false);
            }

        btn_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                tv_final_marks.setText("");
                count=0;

                if(sheet_count>0) {
                    sheet_count--;

                    PrevButton();
                    tv_page_no.setText("" + (sheet_count + 1));

                    if (answerSheetModelList.get(sheet_count + 1).evaluatedMarks.equals("null")) {
                        marks= "" + (Integer.parseInt(tv_evaluated_marks.getText().toString()) - currentTotalMarks);
                        tv_evaluated_marks.setText(marks);
                    }
                    else
                    {
                        marks= "" + (Integer.parseInt(tv_evaluated_marks.getText().toString()) - Integer.parseInt(answerSheetModelList.get(sheet_count + 1).evaluatedMarks));
                        tv_evaluated_marks.setText(marks);
                    }
                    }
                    else {

                    String initial_marks=answerSheetModelList.get(sheet_count).evaluatedMarks;
                    if(initial_marks.equals("null")) {
                        tv_evaluated_marks.setText("0");
                    }
                    else
                    {
                        tv_evaluated_marks.setText(initial_marks);
                    }
                }

                int childCount = me.getChildCount();
                for(int i = 0; i < childCount; i++){
                    if (me.getChildAt(i).getVisibility() == View.VISIBLE) {
                        int Id = me.getChildAt(i).getId();
                        me.getChildAt(i).setVisibility(View.GONE);
                        // currentTotalMarks = 0;

                        if( Id == R.id.img_sheet_answerSheetActivity){
                            int a = i;
                            me.getChildAt(a).setVisibility(View.VISIBLE);

                        }
                    }
                }
                tv_current_page_marks.setText("");
                 currentTotalMarks=0;

            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(img_evaluated_url.equals("")) {

                    edt_total_marks.setEnabled(false);
                    ScreenShot();
                }
                else
                {
                    edt_total_marks.setEnabled(false);
                    evaluationSheet();

                }
            }
        });

        switch_status=false;

        switch_digit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

//                if(b)
//                {
//                    Toast.makeText(AnswerSheetActivity.this,"Two Digit",Toast.LENGTH_LONG).show();
//                    switch_status=true;
//                }
//                else
//                {
//                    Toast.makeText(AnswerSheetActivity.this,"One Digit",Toast.LENGTH_LONG).show();
//                    switch_status=false;
//                    count=0;
//                    tv_final_marks.setText("");
//                }
            }
        });


        gestures = (GestureOverlayView) findViewById(R.id.gestures);
        gestures.addOnGesturePerformedListener(this);

        gestures.addOnGestureListener(new GestureOverlayView.OnGestureListener() {
            @Override
            public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {
                x = event.getX();
                y = event.getY();
            }

            @Override
            public void onGesture(GestureOverlayView overlay, MotionEvent event) {

                    overlay.setGestureColor(Color.RED);
            }

            @Override
            public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {


                    DisplayMetrics displayMetrics = new DisplayMetrics();
                    getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                    int height = displayMetrics.heightPixels;
                    int width = displayMetrics.widthPixels;

                    x = width - 150;


                count++;

                if(count>2){
                    count=1;
                }


            }

            @Override
            public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {

            }
        });

    }

    public void allGestureRelease()
    {
        int childCount = me.getChildCount();
        for(int i = 0; i < childCount; i++){
            if (me.getChildAt(i).getVisibility() == View.VISIBLE) {
                int Id = me.getChildAt(i).getId();
                me.getChildAt(i).setVisibility(View.GONE);
                //currentTotalMarks = 0;

                if( Id == R.id.img_sheet_answerSheetActivity){
                    int a = i;
                    me.getChildAt(a).setVisibility(View.VISIBLE);

                }
            }
        }

        currentTotalMarks=0;
        tv_evaluated_marks.setText( reset_all_marks+"");
    }


    public void PrevButton()
    {
        answerSheetModelList= StudentRecyclerAdapter.answerSheetModelList;

        img_url=answerSheetModelList.get(sheet_count).img_full_url;
        img_evaluated_url=answerSheetModelList.get(sheet_count).evaluate_img_full_url;
        uploaded_evaluated_marks=answerSheetModelList.get(sheet_count).evaluatedMarks;
        uploaded_total_marks=answerSheetModelList.get(sheet_count).totalMark;


        if(!img_evaluated_url.equals(""))
        {

           // Picasso.with(AnswerSheetActivity.this).invalidate(img_evaluated_url);
            Glide.with(AnswerSheetActivity.this)
                    .load(img_evaluated_url)
                    .asBitmap()
                    .error(R.drawable.default_bg)
                    .placeholder(R.drawable.default_bg)
                    .into(img_view_answerSheet);
            // imageLoader.DisplayImage(img_evaluated_url,img_view_answerSheet);


        }
        else
        {
            Glide.with(AnswerSheetActivity.this)
                    .load(img_url)
                    .error(R.drawable.default_bg)
                    .placeholder(R.drawable.default_bg)
                    .into(img_view_answerSheet);
            // imageLoader.DisplayImage(img_url,img_view_answerSheet);
        }


    }

    public void evaluationSheet()
    {
        sheet_count++;

        if(sheet_count<answerSheetModelList.size()) {
            img_url=answerSheetModelList.get(sheet_count).img_full_url;
            img_evaluated_url=answerSheetModelList.get(sheet_count).evaluate_img_full_url;

            //uploaded_evaluated_marks=answerSheetModelList.get(sheet_count).evaluatedMarks;
            uploaded_total_marks=answerSheetModelList.get(sheet_count).totalMark;
            uploaded_evaluated_marks = "0";


            if(!img_evaluated_url.equals("")) {

                for (int i = 0; i <= sheet_count; i++) {
                    uploaded_evaluated_marks = "" + (Integer.parseInt(uploaded_evaluated_marks) + Integer.parseInt(answerSheetModelList.get(i).evaluatedMarks));
                    eval_marks=uploaded_evaluated_marks;
                }
            }
            else
            {
                uploaded_evaluated_marks=tv_evaluated_marks.getText().toString();
            }
            if (!img_evaluated_url.equals("")) {
                Glide.with(AnswerSheetActivity.this)
                        .load(img_evaluated_url)
                        .into(img_view_answerSheet);
                // imageLoader.DisplayImage(img_evaluated_url,img_view_answerSheet);
            } else {
                Glide.with(AnswerSheetActivity.this)
                        .load(img_url)
                        .into(img_view_answerSheet);
                // imageLoader.DisplayImage(img_url,img_view_answerSheet);
            }

            tv_page_no.setText("" + (sheet_count + 1));

            if(!uploaded_evaluated_marks.equals("null")) {
                tv_evaluated_marks.setText(uploaded_evaluated_marks);
                reset_all_marks=Integer.parseInt(uploaded_evaluated_marks);
            }
            if(!uploaded_total_marks.equals("null")) {
                edt_total_marks.setText(uploaded_total_marks);
            }
        }
        else
        {
            sheet_count--;
            alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!","All Sheet Evaluated",true,1);


           // alertFinish();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();

        tv_total_page.setText(""+total_sheet);
        tv_page_no.setText(""+(sheet_count+1));
    }

    @Override
    public void onGesturePerformed(GestureOverlayView overlay, Gesture gesture) {
        ArrayList<Prediction> predictions = mLibrary.recognize(gesture);

        if (predictions.size() > 0 && predictions.get(0).score > 1.0) {
            result = predictions.get(0).name;

            overlay.addOnGestureListener(new GestureOverlayView.OnGestureListener() {
                @Override
                public void onGestureStarted(GestureOverlayView overlay, MotionEvent event) {


                    x = event.getX();
                    y = event.getY();

                }

                @Override
                public void onGesture(GestureOverlayView overlay, MotionEvent event) {

                }

                @Override
                public void onGestureEnded(GestureOverlayView overlay, MotionEvent event) {

                }

                @Override
                public void onGestureCancelled(GestureOverlayView overlay, MotionEvent event) {


                }
            });


            if(count == 1){
                result1=result;
                tv_final_marks.setText(result1);
                pass = true;
                isPass=true;

                if(bool1) {

                    new CountDownTimer(500, 10) {

                        @Override
                        public void onTick(long millisUntilFinished) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public void onFinish() {
                            // TODO Auto-generated method stub


                            if(isPass) {

                                isPass=false;
                                DisplayMetrics displayMetrics = new DisplayMetrics();
                                getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
                                int height = displayMetrics.heightPixels;
                                int width = displayMetrics.widthPixels;

                                x = width - 150;


                                RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(
                                        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                                if (img_evaluated_url.equals("") && final_currentTotalMarks <= final_total) {
                                    text = new TextView(AnswerSheetActivity.this);
                                    text.setLayoutParams(lparams);

                                    text.setX(x);
                                    text.setY(y);
                                    text.setText(result);
                                    text.setTextColor(Color.RED);
                                    text.setTypeface(null, Typeface.BOLD);
                                    text.setTextSize(30);
                                    AnswerSheetActivity.this.me.addView(text);

                                    tv_final_marks.setText("");
                                }
                                count=0;


                                if(result!=null){
                                    bool = true;
                                    tempresult = Integer.parseInt(result);

                                    //currentMarks.setText( currentTotalMarks+"");


                                    if (img_evaluated_url.equals("")) {
                                        bool = true;
                                        tempresult = Integer.parseInt(result.toString());
                                        int eval_marks = Integer.parseInt(tv_evaluated_marks.getText().toString());

                                        final_total = Integer.parseInt(edt_total_marks.getText().toString());

                                        final_currentTotalMarks = eval_marks + tempresult;


                                        if (final_currentTotalMarks <= final_total) {
                                            tv_evaluated_marks.setText(final_currentTotalMarks + "");

                                            currentTotalMarks = currentTotalMarks + tempresult;
                                            tv_current_page_marks.setText("" + currentTotalMarks);
                                        } else {
                                            text.setVisibility(View.GONE);
                                            if (bool) {
                                                final_currentTotalMarks = final_currentTotalMarks - tempresult;
                                                tv_evaluated_marks.setText(final_currentTotalMarks + "");
                                                tv_current_page_marks.setText("" + currentTotalMarks);
                                                bool = false;
                                            }
                                            Toast.makeText(AnswerSheetActivity.this, "Evaluated Marks must be less then Total Marks", Toast.LENGTH_LONG).show();
                                            //  final_result = "";
                                        }
                                    }
                                    else
                                    {
                                        currentTotalMarks = currentTotalMarks + Integer.parseInt(result);
                                    }


                                }


                            }
                            // Toast.makeText(MainActivity.this,"hello",Toast.LENGTH_LONG).show();
                            bool1=true;
                            result = "";


                        }
                    }.start();
                    bool1=false;
                }

            }
            else if(count>=2 && !pass)
            {
                Toast.makeText(AnswerSheetActivity.this,"Please wait till first digit disappear",Toast.LENGTH_SHORT).show();
            }

            else if(count == 2 && pass) {

                isPass=false;

                tv_final_marks.setText("");

                String finalResult = result1 + result;

                RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(
                        RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

                if (img_evaluated_url.equals("") && final_currentTotalMarks <= final_total) {
                    text = new TextView(AnswerSheetActivity.this);
                    text.setLayoutParams(lparams);

                    text.setX(x);
                    text.setY(y);
                    text.setText(finalResult);
                    text.setTextColor(Color.RED);
                    text.setTypeface(null, Typeface.BOLD);
                    text.setTextSize(30);
                    AnswerSheetActivity.this.me.addView(text);
                }


                count = 0;
                pass = false;



                if(finalResult!=null){
                    bool = true;
                    tempresult = Integer.parseInt(finalResult);

                    //currentMarks.setText( currentTotalMarks+"");


                    if (img_evaluated_url.equals("")) {
                        bool = true;
                        tempresult = Integer.parseInt(finalResult.toString());
                        int eval_marks = Integer.parseInt(tv_evaluated_marks.getText().toString());

                        final_total = Integer.parseInt(edt_total_marks.getText().toString());

                        final_currentTotalMarks = eval_marks + tempresult;


                        if (final_currentTotalMarks <= final_total) {
                            tv_evaluated_marks.setText(final_currentTotalMarks + "");

                            currentTotalMarks = currentTotalMarks + tempresult;
                            tv_current_page_marks.setText("" + currentTotalMarks);
                        } else {
                            text.setVisibility(View.GONE);
                            if (bool) {
                                final_currentTotalMarks = final_currentTotalMarks - tempresult;
                                tv_evaluated_marks.setText(final_currentTotalMarks + "");
                                tv_current_page_marks.setText("" + currentTotalMarks);
                                bool = false;
                            }
                            Toast.makeText(AnswerSheetActivity.this, "Evaluated Marks must be less then Total Marks", Toast.LENGTH_LONG).show();
                            //  final_result = "";
                        }
                    }
                    else
                    {
                        currentTotalMarks = currentTotalMarks + Integer.parseInt(finalResult);
                        tv_evaluated_marks.setText(final_currentTotalMarks + "");
                    }

                }
            }

        }
    }

    void ScreenShot(){

        View v1 = screenshot.getChildAt(0);
        v1.setDrawingCacheEnabled(true);
        Bitmap bm = v1.getDrawingCache();
        BitmapDrawable bitmapDrawable = new BitmapDrawable(bm);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
        byteArray = stream.toByteArray();

        String student_id,school_grade_section_id,barcode_id,img_srl_no,created_date;
        String evaluated_by,total_mark,access_token,base64Url,evaluated_marks;

        loginModel = loginSession.getPreferences(AnswerSheetActivity.this, AppConstant.login_session);

        if(loginModel!=null)
        {
          evaluated_by=loginModel.user_id;
          access_token=loginModel.access_token;

            base64Url= Base64.encodeToString(byteArray, Base64.DEFAULT);

            student_id=answerSheetModelList.get(sheet_count).user_id;
            school_grade_section_id=answerSheetModelList.get(sheet_count).school_grade_section_id;
            barcode_id=answerSheetModelList.get(sheet_count).barcode_id;
            img_srl_no=answerSheetModelList.get(sheet_count).img_srl_no;
            created_date=answerSheetModelList.get(sheet_count).createdDate;

            evaluated_marks=tv_current_page_marks.getText().toString();
            total_mark=edt_total_marks.getText().toString();

            if(evaluated_marks.equals(""))
            {
                evaluated_marks="0";
            }

            if(sheet_count<total_sheet) {

                if(evaluated_marks.equals("0") || total_mark.equals("") || total_mark.equals("0"))
                {
                      if(total_mark.equals("") || total_mark.equals("0")) {
                          edt_total_marks.setEnabled(true);
                          alertMessage.showAlertDialog(AnswerSheetActivity.this, "Alert!", "Total Marks Should not be blank", false);

                      }
                    else if(evaluated_marks.equals("0"))
                    {
                        alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!","Please put Marks first",false);
                    }

                }
                else if(Integer.parseInt(evaluated_marks)>Integer.parseInt(total_mark))
                {
                    alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!","Evaluated Marks should not be greater then total marks",false);
                }
                else {

                    UploadSheetApi uploadSheetApi=new UploadSheetApi(student_id,school_grade_section_id,barcode_id,
                            img_srl_no,created_date,base64Url,evaluated_marks,evaluated_by,total_mark,access_token);

                    uploadSheetApi.execute();
                }
            }
            else {
                alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!","All Sheet Evaluated",true,1);

               // alertFinish();
            }

        }

    }

    public void alertFinish()
    {
        AlertDialog.Builder alert=new AlertDialog.Builder(AnswerSheetActivity.this);
        alert.setTitle("Alert");
        alert.setMessage("All sheet evaluated.");
        alert.setCancelable(false);
        alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                finish();
            }
        });

        AlertDialog alertDialog=alert.create();
        alertDialog.show();
    }

    /*......................calling the api for Upload Sheet....................................*/
    public class UploadSheetApi extends AsyncTask<Void,Void,JSONObject>
    {

        String access_token, message, status_code, status;
        String student_id,school_grade_section_id,barcode_id,img_srl_no,created_date;
        String evaluated_by,total_mark,login_access_token,base64Url,evaluated_marks;

         UploadSheetApi(String student_id, String school_grade_section_id,String barcode_id,
                String img_srl_no,String created_date,String base64Url,String evaluated_marks,
                String evaluated_by,String total_mark,String login_access_token)
        {
            this.student_id=student_id;
            this.school_grade_section_id=school_grade_section_id;
            this.barcode_id=barcode_id;
            this.img_srl_no=img_srl_no;
            this.created_date=created_date;
            this.base64Url=base64Url;
            this.evaluated_marks=evaluated_marks;
            this.evaluated_by=evaluated_by;
            this.total_mark=total_mark;
            this.login_access_token=login_access_token;
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            HashMap<String,String> data=new HashMap<>();

            JSONObject jsonObject=null;
            data.put("UserID",student_id);
            data.put("SchoolGradeSectionAssociationID",school_grade_section_id);
            data.put("BarcodeID",barcode_id);
            data.put("ImgSrno",img_srl_no);
            data.put("CreatedDate",created_date);
            data.put("EvaluationImgURL",base64Url);
            data.put("EvaluatedMarks",evaluated_marks);
            data.put("EvaluatedBy",evaluated_by);
            data.put("TotalMark",total_mark);
            data.put("AccessToken",login_access_token);
            try {

                jsonObject = handler.makeHttpRequest(AppConstant.evaluation_marks_url, AppConstant.post_method, data);

                Log.d("login response.....", jsonObject.toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                AnswerSheetActivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!!","Slow Internet..Timed Out",false);
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();

            try
            {
                JSONObject security_info=jsonObject.getJSONObject("SecurityInfo");

                message=security_info.getString("Message");
                access_token=security_info.getString("AccessToken");
                status_code=security_info.getString("StatusCode");
                status=security_info.getString("Status");

                if(status==AppConstant.isSuccess)
                {
                    evaluationSheet();
                    allGestureRelease();
                    Toast.makeText(AnswerSheetActivity.this,"Sheet Evaluated",Toast.LENGTH_LONG).show();
                }
                else
                {
                    alertMessage.showAlertDialog(AnswerSheetActivity.this,"Alert!!","Answer Sheet could not save",false);
                }

            }
            catch (JSONException ex)
            {
                ex.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }
}
