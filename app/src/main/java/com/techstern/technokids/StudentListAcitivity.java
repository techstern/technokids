package com.techstern.technokids;

import android.app.ProgressDialog;
import android.app.VoiceInteractor;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.menu.ListMenuItemView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.picasso.Picasso;
import com.techstern.technokids.adapter.StudentRecyclerAdapter;
import com.techstern.technokids.model.LoginModel;
import com.techstern.technokids.model.StudentListModel;
import com.techstern.technokids.services.JsonObjectHandler;
import com.techstern.technokids.session.LoginSession;
import com.techstern.technokids.utility.AlertMessage;
import com.techstern.technokids.utility.AppConstant;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

public class StudentListAcitivity extends AppCompatActivity {

    LoginModel loginModel;
    String header_user_name,header_user_image,header_user_role;
    String user_id,access_token;
    ProgressDialog progressDialog;
    JsonObjectHandler handler;
    AlertMessage alertMessage;
    StudentListModel studentListModel;
    List<StudentListModel> studentListModelList;
    StudentRecyclerAdapter studentRecyclerAdapter;

    RecyclerView list_student;
    Button btn_logout;
    TextView tv_user_name,tv_user_role;
    ImageView img_user;
    EditText edt_search;

    LoginSession loginSession;

    String student_list_data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_list_acitivity);

        initView();
    }

    public void initView()
    {
        list_student=(RecyclerView)findViewById(R.id.list_studentListActivity);
        tv_user_name=(TextView)findViewById(R.id.tv_user_name_studentListActivity);
        tv_user_role=(TextView)findViewById(R.id.tv_user_role_studentListActivity);
        btn_logout=(Button) findViewById(R.id.btn_log_out_studentListActivity);
        img_user=(ImageView)findViewById(R.id.img_user_studentListActivity);
        edt_search=(EditText)findViewById(R.id.edt_search_studentListActivity);

        //initialization
        loginModel=new LoginModel();
        loginSession=new LoginSession();
        progressDialog=new ProgressDialog(this);
        handler=new JsonObjectHandler();
        alertMessage=new AlertMessage();
        studentListModelList=new ArrayList<>();

        loginModel = loginSession.getPreferences(StudentListAcitivity.this, AppConstant.login_session);






        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginSession.setPreferences(StudentListAcitivity.this,AppConstant.login_session,null);
                finishAffinity();

                startActivity(new Intent(StudentListAcitivity.this,LoginActivity.class));
            }
        });

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String text = edt_search.getText().toString().toLowerCase(Locale.getDefault());
                studentRecyclerAdapter.filter(text);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        if(loginModel!=null)
        {
            header_user_name=loginModel.full_name;
            header_user_role=loginModel.roll_name;
            header_user_image=loginModel.user_full_image;
            access_token=loginModel.access_token;
            user_id=loginModel.user_id;

            tv_user_name.setText("Welcome, "+header_user_name);
            tv_user_role.setText(header_user_role);

            Picasso.with(StudentListAcitivity.this)
                    .load(header_user_image)
                    .into(img_user);

            if(access_token!=null) {
                StudentListApi studentListApi = new StudentListApi(user_id, access_token);
                studentListApi.execute();
            }
            else
            {
                alertMessage.showAlertDialog(StudentListAcitivity.this,"Alert","Session Expired",false);
            }

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAffinity();
    }

    /*......................calling the api for login....................................*/
    public class StudentListApi extends AsyncTask<Void,Void,JSONObject>
    {
        String school_id, student_name, grade_name, section_name, barcode_id;
        String school_grade_section_association_id, user_id, img_serial_no;
        String evaluated_marks, total_marks, status_obj, created_date,grade_section;
        String access_token,student_id;
        String student_list_data;
        String message, status_code, status,student_access_token;

        StudentListApi(String user_id,String access_token)
        {
            this.user_id=user_id;
            this.access_token=access_token;

            student_list_data="?UserID="+user_id+ "&AccessToken"+access_token;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog.setMessage("loading");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected JSONObject doInBackground(Void... params) {

            HashMap<String,String> data=new HashMap<>();

            JSONObject jsonObject=null;
            try {

                jsonObject = handler.makeHttpRequest(AppConstant.student_list_url+student_list_data, AppConstant.get_method, data);

                Log.d("login response.....", jsonObject.toString());
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
                StudentListAcitivity.this.runOnUiThread(new Runnable() {
                    public void run() {
                        alertMessage.showAlertDialog(StudentListAcitivity.this,"Alert!!","Slow Internet..Timed Out",false);
                    }
                });
            }
            return jsonObject;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);
            progressDialog.dismiss();

            try
            {
                JSONObject security_info=jsonObject.getJSONObject("SecurityInfo");

                message=security_info.getString("Message");
                student_access_token=security_info.getString("AccessToken");
                status_code=security_info.getString("StatusCode");
                status=security_info.getString("Status");

                if(status==AppConstant.isSuccess)
                {
                    JSONArray accessInformation=jsonObject.getJSONArray("answerSheet");

                    studentListModelList.clear();

                    for(int i=0;i<accessInformation.length();i++) {

                        JSONObject accessInfo = accessInformation.getJSONObject(i);

                        school_id = accessInfo.getString("SchoolID");
                        student_name = accessInfo.getString("StudentName");
                        grade_name = accessInfo.getString("GradeName");
                        section_name = accessInfo.getString("SectionName");
                        school_grade_section_association_id = accessInfo.getString("SchoolGradeSectionAssociationID");
                        barcode_id = accessInfo.getString("BarcodeID");
                        student_id = accessInfo.getString("UserID");
                        img_serial_no = accessInfo.getString("ImgSrno");
                        evaluated_marks = accessInfo.getString("EvaluatedMarks");
                        total_marks = accessInfo.getString("TotalMark");
                        status = accessInfo.getString("Status");
                        created_date = accessInfo.getString("CreatedDate");

                        studentListModel = new StudentListModel();

                        studentListModel.school_id = school_id;
                        studentListModel.student_name = student_name;
                        studentListModel.grade_name = grade_name;
                        studentListModel.section_name = section_name;
                        studentListModel.school_grade_section_association_id = school_grade_section_association_id;
                        studentListModel.barcode_id = barcode_id;
                        studentListModel.student_id = student_id;
                        studentListModel.img_serial_no = img_serial_no;
                        studentListModel.evaluated_marks = evaluated_marks;
                        studentListModel.total_marks = total_marks;
                        studentListModel.task_status = status;
                        studentListModel.created_date = created_date;
                        studentListModel.grade_section=grade_name+" "+section_name;

                        studentListModelList.add(studentListModel);
                    }

                    studentRecyclerAdapter = new StudentRecyclerAdapter(studentListModelList,StudentListAcitivity.this);

                    LinearLayoutManager verticalLayoutmanager
                            = new LinearLayoutManager(StudentListAcitivity.this, LinearLayoutManager.VERTICAL, false);

                    list_student.setLayoutManager(verticalLayoutmanager);

                    list_student.setAdapter(studentRecyclerAdapter);
                }
            }
            catch (JSONException ex)
            {
                ex.printStackTrace();
            }
            catch (Exception ex)
            {
                ex.printStackTrace();
            }

        }
    }






}
